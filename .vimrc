set nu ts=4 sw=4 ai mouse=a
map <C-B> <ESC>:!g++ % -o %< -Wall -O3 -std=c++11<CR>

filetype plugin on
colorscheme molokai
set t_Co=256
syntax on

execute pathogen#infect()
set guifont=DejaVu\ Sans\ mono\ 11
